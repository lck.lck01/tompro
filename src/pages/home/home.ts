import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { MyHobbyProvider } from '../../providers/my-hobby/my-hobby';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private ser: MyHobbyProvider) {
  }

  items = [];

  ionViewDidLoad() {
    this.ser.getData();
  }

  ionViewDidEnter() {
    for ( var i = 0; i < this.ser.itemData.length; i++) {
      this.items.push(this.ser.itemData[i]);
    }
  }

}
