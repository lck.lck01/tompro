import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MyHobbyProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MyHobbyProvider {

  constructor(private http: Http) {
  }

  private url = 'http://localhost:3000/hobbies';

  itemData = [];

    getData() {
      this.http.get(this.url)
      .map(res => res.json())
      .subscribe(
        data => {
          for ( var i = 0; i < data.length; i++) {
            this.itemData.push(data[i].name);
          }
        }
      )
    }



}
